from __future__ import annotations

from typing import TYPE_CHECKING

from caverns_of_wintermoor.components.base_component import BaseComponent
from caverns_of_wintermoor.equipment_types import EquipmentType

if TYPE_CHECKING:
    from caverns_of_wintermoor.entity import Item


class Equippable(BaseComponent):
    parent: Item

    def __init__(
        self,
        equipment_type: EquipmentType,
        power_bonus: int = 0,
        defense_bonus: int = 0,
    ) -> None:
        self.equipment_type = equipment_type

        self.power_bonus = power_bonus
        self.defense_bonus = defense_bonus


class Dagger(Equippable):
    def __init__(self) -> None:
        super().__init__(
            equipment_type=EquipmentType.WEAPON,
            power_bonus=1,
        )


class Sword(Equippable):
    def __init__(self) -> None:
        super().__init__(
            equipment_type=EquipmentType.WEAPON,
            power_bonus=2,
        )


class GreatSword(Equippable):
    def __init__(self) -> None:
        super().__init__(
            equipment_type=EquipmentType.WEAPON,
            power_bonus=3,
        )


class MagicDagger(Equippable):
    def __init__(self) -> None:
        super().__init__(
            equipment_type=EquipmentType.WEAPON,
            power_bonus=4,
        )


class MagicSword(Equippable):
    def __init__(self) -> None:
        super().__init__(
            equipment_type=EquipmentType.WEAPON,
            power_bonus=5,
        )


class MagicGreatsword(Equippable):
    def __init__(self) -> None:
        super().__init__(
            equipment_type=EquipmentType.WEAPON,
            power_bonus=6,
        )


class LeatherArmor(Equippable):
    def __init__(self) -> None:
        super().__init__(equipment_type=EquipmentType.ARMOR, defense_bonus=1)


class ChainMail(Equippable):
    def __init__(self) -> None:
        super().__init__(
            equipment_type=EquipmentType.ARMOR,
            defense_bonus=3,
        )


class PlateArmor(Equippable):
    def __init__(self) -> None:
        super().__init__(
            equipment_type=EquipmentType.ARMOR,
            defense_bonus=5,
        )
