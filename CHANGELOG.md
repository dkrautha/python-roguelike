# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.3.0] - 2021-05-13

### Added

- Small message telling the player how to quit after victory
- README:
  - Keybinding to descend the staircase

### Changed

- Max number of spawns per room
  - Floor 1: 1 item, 2 enemies
  - Floor 3: 2 items, 3 enemies
  - Floor 5: 3 items, 4 enemies
  - Floor 6: 3 items, 5 enemies
  - Floor 7: 4 items, 5 enemies
- Enemy spawn weightings
  - Floor 4: 25/20/55 kobold, goblin, orc
  - Floor 5: 10/10/40/20 kobold, goblin, orc, troll
  - Floor 6: 5/5/40/20/20 kobold, goblin, orc, troll, hobgoblin
  - Floor 7: 0/0/35/30/30 kobold, goblin, orc, troll, hobgoblin
- Item spawn weightings
  - Floor 5: chain-mail weight 15 -> 5
  - Floor 8: plate armor weight 15 -> 5
- Damage calculation ensures 1 damage is always dealt
  - Prior to this the player could become invincible with enough level investment into agility
- Hobgoblin color to a more visible blue instead of orange
- Project structure refactor
  - Renamed the base module from python_roguelike to caverns_of_wintermoor
  - Renamed all imports to reflect this change
  - Changed cli command to run the game to "caverns"
- Updated project dependencies
  - Reflected in new poetry.lock and requirements.txt
- README:
  - Change title to reflet the game's name

### Fixed

- Inventory slots after v not being selectable
- Small typos in consumables.py
- A new floor no longer generates after the player wins
- Confused enemies not being able to move southwest
- Help menu not having a the descend stairs keybind

## [0.2.1] - 2021-05-10

### Added

- In-game help menu brought up with ? key
- Basic win condition for ending the game on reaching the 10th floor staircase
- An official name! This game is now called "Caverns of Wintermoor"!

### Changed

- Title of the created window reflects new name
- Main menu reflects new title
- Prior calls to os.path now use Path from pathlib
  - I/O operations are now much easier to understand and should have the same functionality as before
- Update dependencies via poetry
  - poetry.lock and requirements.txt reflect these updates
- README:
  - Remove complete TODO's
  - Change general info description
  - Add ? key to keybindings
  - Add win condition to features

### Fixed

- Giants should now have a "G" character instead of "D"
- A few random typos in docstrings

## [0.2.0] - 2021-05-02

### Added

- New low level enemy types
  - Goblin: 6hp, 0df, 3pw, 35xp
  - Kobold: 8hp, 0df, 4pw, 55xp
- New mid-high level enemy types
  - Hobgoblin: 20hp, 2df, 4pw, 115xp
  - Giant: 30hp, 4df, 8pw, 140xp
  - Dragon: 50hp, 6df, 10pw, 300xp
- New weapon types
  - Great Sword: power bonus of 3
  - Magic Variants: +3 additional buff over non-magical counterparts
    - These are not currently obtainable in game
- New armor types
  - Plate Armor: defense bonus of 5
- This changelog!

### Changed

- Power buff of dagger reduced to 1
- Power buff of sword reduced to 2
- Orcs now have 12hp, 4pw, 75xp
- Trolls now have 16hp, 1df, 6pw, 100xp
- Three weapon types now have different colors
- Three armor types now have different colors
- Enemy and Item spawn weightings
  - Floor 1: 50/50 kobold, goblin
  - Floor 3: 50/50/45 kobold, goblin, orc
  - Floor 4: 25/20/55/20 kobold, goblin, orc, troll
  - Floor 5: 10/10/40/50/50 kobold, goblin, orc, troll, hobgoblin
  - Floor 7: 0/0/40/80/80 kobold, goblin, orc, troll, hobgoblin
  - Floor 8: 40/80/80/20 orc, troll, hobgoblin, giant
  - Floor 9: 40/80/80/40 orc, troll, hobgoblin, giant
  - Floor 10: 40/80/80/80/20 orc, troll, hobgoblin, giant, dragon
- README:
  - Remove completed TODO's
  - Add g key to drop items
  - Change the wording of the status section

### Fixed

- Fix number number of monsters spawning per room always being the same as the number of items.
- Fix chain mail armor being treated as a weapon and granting a power bonus instead of a defense bonus.
- Fix choosing a power increase at level up increasing defense instead of power

## [0.1.0] - 2021-04-30

### Added

- A GitLab tag to mark this as the first official release.
- Two enemy types (Orcs and Trolls).
- A way to save the game state.
- Two types of enemies
  - Orc (weaker)
  - Troll (stronger)
- Three types of scrolls
  - Fire scrolls (targeted AOE effect)
  - Lightning scrolls (zaps the nearest enemy)
  - Confusion scrolls (causes an enemy to meander about for a few turns)
- Two types of equipment
  - Weapons (dagger and sword)
  - Armor (leather and chain mail)
- Consumables (health potion).
- A 26 item inventory.
- Way too many other things, ie. building the entire base of the game.
